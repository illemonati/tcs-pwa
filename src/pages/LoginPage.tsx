import React, { useEffect, useState } from "react";
import { sha3_256 } from "js-sha3";

import {
    Box,
    Button,
    Container,
    Fab,
    TextField,
    Typography,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";

const LoginPage: React.FC = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [hash, setHash] = useState("");

    useEffect(() => {
        if (!username || !password) {
            setHash("");
            return;
        }
        const combined = JSON.stringify({
            username: username,
            password: password,
        });
        setHash(sha3_256(combined));
    }, [username, password]);

    return (
        <Container
            sx={{
                px: {
                    xs: "10%",
                },
            }}
            maxWidth="md"
        >
            <Grid
                sx={{ paddingTop: "30%", height: "100%", width: "100%" }}
                container
                gap={3}
            >
                <Grid xs={12} sx={{ paddingBottom: "10%" }}>
                    <Typography variant="h4">TCS</Typography>
                    <Typography variant="subtitle2">
                        A Chat Messager Created by STIC at VT
                    </Typography>
                </Grid>
                <Grid sx={{ textAlign: "left" }} xs={12}>
                    <Typography variant="subtitle2">Username</Typography>
                    <TextField
                        variant="outlined"
                        placeholder="Username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        fullWidth
                    />
                </Grid>
                <Grid sx={{ textAlign: "left" }} xs={12}>
                    <Typography variant="subtitle2">Password</Typography>
                    <TextField
                        variant="outlined"
                        placeholder="Password"
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        fullWidth
                    />
                </Grid>
                <Grid
                    sx={{ textAlign: "left", overflowWrap: "anywhere" }}
                    xs={12}
                >
                    <Typography variant="body2">{hash}</Typography>
                </Grid>
                <Grid xs={12} sx={{ paddingTop: "0.5rem" }}>
                    <Button
                        disableElevation
                        variant="contained"
                        sx={{ borderRadius: 8 }}
                    >
                        Login
                    </Button>
                </Grid>
                <Grid xs={12}>
                    <Typography variant="subtitle2">
                        Registration is not required, simply log in.
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    );
};

export default LoginPage;
