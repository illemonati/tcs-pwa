import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import React from "react";
import "./App.css";
import LoginPage from "./pages/LoginPage";
import { isMobile } from "react-device-detect";
import MobileWebHomePage from "./pages/MobileWebHomePage";

const theme = createTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 900,
            lg: 1200,
            xl: 1536,
        },
    },
});

const isInStandaloneMode = () =>
    window.matchMedia("(display-mode: standalone)").matches ||
    (window.navigator as any).standalone ||
    document.referrer.includes("android-app://");

function App() {
    return (
        <div className="App">
            <CssBaseline />
            <ThemeProvider theme={theme}>
                {isMobile && !isInStandaloneMode() ? (
                    <MobileWebHomePage />
                ) : (
                    <LoginPage />
                )}
            </ThemeProvider>
        </div>
    );
}

export default App;
